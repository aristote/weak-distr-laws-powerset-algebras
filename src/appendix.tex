\begin{toappendix}
  \section{Additional tool for the proofs: semi-algebras}
  \label{sec:apx:proof-preliminaries}

  Call a \emph{semi-monad} on a category $\catC$ the data $\semimonadT$ of an
  endofunctor $\funcT: \catC \to \catC$ and a natural transformation $\multT:
  \funcTT \to \funcT$ such that $\multT \circ \funcT \multT = \multT \circ
  \multT \funcT$. In particular, any monad has an underlying semi-monad, weak
  distributive laws are really distributive laws between a monad and a semi-monad,
  and weak extensions of monads are just extensions of the underlying semi-monads.

  Given a semi-monad $\funcT$, one may form the category $\SemiEMT$ of its
  \emph{semi-algebras}: a semi-$\funcT$-algebra is a pair $\alg{A}{a}$ of an
  object $A$ of $\catC$ and of an arrow $a: \funcT A \to A$ such that $a \circ
  \multT_A = a \circ \funcT a$, and a semi-$\funcT$-algebra morphism $\alg{A}{a}
  \to \alg{B}{b}$ is an arrow $f: A \to B$ in $\catC$ such that $f \circ a = b
  \circ \funcT f$ --- the identity morphism is the one with the identity as its
  underlying $\catC$-arrow, and composition of morphisms is done by composing
  the underlying $\catC$-arrows. Just like for $\EMT$, there are functors
  $\adjunction{\catC}{\SemiEMT}{\leftSemiEMT}{\rightSemiEMT}$ such that
  $\rightSemiEMT \leftSemiEMT = \funcT$, so that $\leftSemiEMT$ sends a
  semi-algebra $\alg{A}{a}$ to its carrier object $A$ and a morphism $\alg{A}{a}
  \to \alg{B}{b}$ to the underlying arrow $A \to B$, while $\rightSemiEMT$ sends
  an object $X$ to the free $\funcT$-algebra on $X$, given by the pair
  $\alg{\funcT X}{\multT_X}$, and an arrow $f: X \to Y$ to the morphism
  $\alg{\funcT X}{\multT_X} \to \alg{\funcT Y}{\multT_Y}$ with underlying
  $\catC$-arrow $\funcT f: \funcT X \to \funcT Y$. There is a natural
  transformation $\counitT: \leftSemiEMT \rightSemiEMT \to \Id$ (in $\SemiEMT$)
  given by $\rightSemiEMT \counitT_{\alg{A}{a}} = a: \funcT A \to A$.

  If $\funcT$ is also a monad, we let $\EMT$ be the full subcategory of $\SemiEMT$
  on the $\funcT$-algebras, i.e. semi-$\funcT$-algebras $\alg{A}{a}$ such that $a
  \circ \unitT_A = \id_A$. We write $\SemiT$ for the inclusion $\EMT \to
  \SemiEMT$, and $\leftEMT$ and $\rightEMT$ are the restrictions of $\leftSemiEMT$
  and $\rightSemiEMT$ to $\EMT$.

  In general if $\funcU: \catD \to \catC$ is a functor, a \emph{lifting} of an
  endofunctor $\funcF: \catC \to \catC$ along $\funcU$ is an endofunctor
  $\lift{\funcF}: \catD \to \catD$ such that $\funcU \lift{\funcF} = \funcF
  \funcU$, while a lifting of a natural transformation $\alpha: \funcF \to
  \funcG$ between endofunctors with liftings $\lift{\funcF}$ and $\lift{\funcG}$
  is a natural transformation $\lift{\alpha}$ such that $\funcU \lift{\alpha} =
  \alpha \funcU$.

  \begin{definition}[lifting to algebras]
    \label{def:lifting}
    Let $\funcT$ be a monad on $\catC$. A monad $\monadS$ on $\catC$
    \emph{semi-lifts} to $\SemiEMT$ when $\funcT$, $\unitT$ and $\multT$ lift
    along $\rightSemiEMT$. It \emph{lifts} to $\EMT$ when it has a semi-lifting to
    $\SemiEMT$ that restricts to $\EMT$.
  \end{definition}

  Giving a lifting of an endofunctor $\funcS: \catC \to \catC$ along
  $\rightSemiEMT$ is equivalent to giving a law $\distrTS$ having
  \Cref{cd:mult+} commute: the law $\distrTSname$ induces the semi-lifting which
  sends the semi-$\funcT$-algebra $(A,a)$ to $\alg{\funcS A}{\funcTS A
    \xrightarrow{\rho_A} \funcST A \xrightarrow{\funcS a} \funcS A}$, and is
  computed from the semi-lifting as $\rho = \rightSemiEMT \counitT \lift{S}
  \leftSemiEMT \circ \funcTS \unitT$. If $\funcS$ and $\funcR$ have liftings
  $\semiliftS$ and $\semiliftR$ along $\rightSemiEMT$, a natural transformation
  $\alpha: \funcS \Rightarrow \funcR$ has a lifting $\semilift{\alpha}$ along
  $\rightSemiEMT$ if and only if $\distrTRname \circ \funcT \alpha = \alpha
  \funcT \circ \distrTSname$. In particular, semi-liftings of a monad $\monadS$
  to $\SemiEMT$ are in one-to-one correspondance with weak distributive laws
  $\distrTStype$.

  Notice the resemblance with the conditions in \Cref{thm:weak-liftings}: this
  is not a coincidence, as we will show next that weak liftings (along
  $\rightEMT$) can be constructed from liftings along $\rightSemiEMT$.

  \begin{wrapfigure}{r}{.3\textwidth}
    \vspace{0pt}
    \begin{tikzcd}[column sep=tiny]
      \SemiEMT && \EMT \\
      & \catC
      \arrow["\SemiT", shift left, from=1-3, to=1-1]
      \arrow["\SplitT", shift left, from=1-1, to=1-3, dashed]
      \arrow["\leftSemiEMT", curve={height=-6pt}, shift left, from=2-2, to=1-1]
      \arrow["\rightSemiEMT", curve={height=6pt}, shift left, from=1-1, to=2-2]
      \arrow["\leftEMT"', curve={height=6pt}, shift right, from=2-2, to=1-3]
      \arrow["\rightEMT"', curve={height=-6pt}, shift right, from=1-3, to=2-2]
    \end{tikzcd}
    \vspace{-25pt}
  \end{wrapfigure}
  Let $\funcT$ be a monad on a category $\catC$ where all idempotents split. It
  then follows that all idempotents in $\SemiEMT$ split as well. Because there
  is an idempotent natural transformation $\closeT: \Id \Rightarrow \Id$ in
  $\SemiEMT$ given by $\rightSemiEMT \closeT = \rightSemiEMT \counitT \circ
  \unitT \rightSemiEMT$, each $\closeT_A$ splits as $\closeT_A = \incT_A \circ
  \projT_A$, and the resulting semi-$\funcT$-algebra in the middle is actually
  an algebra. In fact this construction yields a functor $\SplitT: \SemiEMT \to
  \EMT$ so that $\closeT$ splits as $\projT: \Id \Rightarrow \SemiT \SplitT$
  followed by $\incT: \SemiT \SplitT \Rightarrow \Id$. We then get $\SemiT
  \SplitT f = \projT_B \circ f \circ \incT_A$ for $f: A \to B$ in $\SemiEMT$,
  $\SplitT \SemiT = \Id$ and that $\SplitT$ is both left and right adjoint to
  $\SemiT$: $\incT$ is the counit of $\SemiT \dashv \SplitT$ while $\projT$ is
  the unit of $\SplitT \dashv \SemiT$. To make notations less heavy, if
  $\semiliftS$ is a semi-lifting of $S$, we also write $\projT_\liftS$ for
  $\projT \semiliftS$ and $\incT_\liftS$ for $\incT \semiliftS$. We will also
  omit the superscript $\funcT$ from the functors and natural transformations
  when non-ambiguous.

  If $\semiliftS$ is a lifting of an endofunctor $\funcS$ along $\rightSemiEMT$,
  then $\liftS = \Split \semiliftS \Semi: \EMT \to \EMT$ is a weak lifting of
  $\funcS$ to $\EMT$. Similarly, if $\semilift{\alpha}$ is the lifting of a
  natural transformation $\alpha$ along $\rightSemiEMT$, then $\lift{\alpha} =
  \Split \semilift{\alpha} \Semi$ is a weak lifting of $\alpha$ to $\EMT$. In
  particular, if $\monadS$ has a semi-lifting to $\semiliftS$, then $\liftS$ can
  be made into a monad by composing the adjunction
  $\adjunction{\EMT}{\SemiEMT}{\Semi}{\Split}$ with any adjunction that yields
  $\semiliftS$.
\end{toappendix}
