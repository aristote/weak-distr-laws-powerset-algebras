{
  inputs,
  pkgs,
  ...
}: {
  imports = [inputs.my-nixpkgs.devenvModules.personal];

  languages = {
    nix.enable = true;
    texlive = {
      enable = true;
      packages = tl: {inherit (tl) scheme-full;};
      latexmk = {
        enable = true;
        extraConfig = ''
          @default_files = ("main");
          $ENV{'TEXINPUTS'} = './src//:./lipics//:' . $ENV{'TEXINPUTS'};
          $ENV{'BIBINPUTS'} = './biblio//:' . $ENV{'BIBINPUTS'};
          $clean_ext = 'vtc ' . 'axp' . $clean_ext;
        '';
      };
    };
  };

  gitignore.extra = ''
    ## generated by LIPIcs
    *.vtc
    *.axp
  '';

  packages = with pkgs; [tikzit gnumake42];
}
